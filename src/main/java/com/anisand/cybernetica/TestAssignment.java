package com.anisand.cybernetica;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TestAssignment {
    private static final String E1_MULTIPLE_ROOTS = "E1";
    private static final String E2_CYCLE_PRESENT = "E2";
    private static final String E3_DUPLICATE_NODE = "E3";
    private static final String E4_TOO_MANY_CHILDREN = "E4";
    private static final String E5_GENERAL_ERROR = "E5";

    private static class InvalidBranchException extends RuntimeException {
        private String errorCode;

        InvalidBranchException(String errorCode) {
            this.errorCode = errorCode;
        }

        String getErrorCode() {
            return errorCode;
        }
    }

    private static class Tree {

        private static class Node {
            private String value;
            private Node parent;
            private Node left;
            private Node right;

            Node(String value) {
                this.value = value;
            }
        }

        private Node root;

        Tree(Map<String, Set<String>> branches) {
            root = new Node(findRoot(branches));

            addBranches(root, branches);
        }

        private String findRoot(Map<String, Set<String>> branches) {
            if (branches.isEmpty()) {
                return null;
            }
            Set<String> roots = new HashSet<>(branches.keySet());
            branches.values().forEach(roots::removeAll);

            if (roots.size() > 1) {
                throw new InvalidBranchException(E1_MULTIPLE_ROOTS);
            }

            if (roots.isEmpty()) {
                throw new InvalidBranchException(E2_CYCLE_PRESENT);
            }

            return roots.iterator().next();
        }

        private void addBranches(Node parent, Map<String, Set<String>> branches) {
            if (parent == null) return;

            Set<String> children = branches.get(parent.value);
            if (children == null) return;

            Iterator<String> it = children.iterator();

            if (it.hasNext()) {
                String value = it.next();
                if (contains(value, root)) {
                    throw new InvalidBranchException("E2");
                }
                Node left = new Node(value);
                left.parent = parent;
                parent.left = left;
                addBranches(left, branches);
            }

            if (it.hasNext()) {
                String value = it.next();
                if (contains(value, root)) {
                    throw new InvalidBranchException("E2");
                }
                Node right = new Node(value);
                right.parent = parent;
                parent.right = right;
                addBranches(right, branches);
            }
        }

        private boolean contains(String value, Node root) {
            if (root == null) return false;

            if (root.value.equals(value)) return true;

            if (contains(value, root.left)) return true;

            return contains(value, root.right);
        }

        @Override
        public String toString() {
            StringBuilder sb = new StringBuilder();

            stringify(root, sb);

            return sb.toString();
        }

        private void stringify(Node node, StringBuilder sb) {
            sb.append("(");
            if (node.value != null) sb.append(node.value);
            if (node.left != null) stringify(node.left, sb);
            if (node.right != null) stringify(node.right, sb);
            sb.append(")");
        }
    }

    static String convert(String input) {
        try {
            Map<String, Set<String>> branches = parseInput(input);
            System.out.println("Branches: " + branches);

            Tree tree = new Tree(branches);
            System.out.println("Tree: " + tree);

            return tree.toString();
        } catch (InvalidBranchException e) {
            System.out.println("Error: " + e.getErrorCode());
            return e.getErrorCode();
        } catch (Exception e) {
            System.out.println(e.getClass().getName() + ": " + e.getMessage());
            System.out.println("Error: " + E5_GENERAL_ERROR);
            return E5_GENERAL_ERROR;
        }
    }

    private static Map<String, Set<String>> parseInput(String input) {
        Map<String, Set<String>> branches = new HashMap<>();

        Matcher m = Pattern.compile("\\((.*?)\\)").matcher(input);
        while (m.find()) {
            String[] split = m.group(1).split(",");

            String parent = split[0];
            String child = split[1];

            if (branches.containsKey(parent)) {
                Set<String> set = branches.get(parent);
                if (set.contains(child)){
                    throw new InvalidBranchException(E3_DUPLICATE_NODE);
                } else if (set.size() == 2) {
                    throw new InvalidBranchException(E4_TOO_MANY_CHILDREN);
                }

                branches.get(parent).add(child);
            } else {
                Set<String> newSet = new TreeSet<>();
                newSet.add(child);
                branches.put(parent, newSet);
            }
        }

        return branches;
    }
}
